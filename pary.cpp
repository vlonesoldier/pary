#include <iostream>
#include <fstream>
#include <string>

using namespace std;

bool czyPierwsza(int liczba);
bool czyNp(int liczba);
vector<int> roznica(int liczba);
void wypiszNpPierwsze(ifstream &dataIn, ofstream &dataOut);
void wypiszSlowa(ifstream &dataIn, ofstream &dataOut);
void wypiszPare(ifstream &dataIn, ofstream &dataOut);

int main()
{
    ifstream dataIn;
    ofstream dataOut("wyniki.txt");

    dataIn.open("./Dane_PR2/pary.txt");
    wypiszNpPierwsze(dataIn, dataOut);
    wypiszSlowa(dataIn, dataOut);
    wypiszPare(dataIn, dataOut);

    dataIn.close();
    dataOut.close();

    return 0;
}

bool czyPierwsza(int liczba)
{
    for (int i = 2; i < liczba; i++)
    {
        if (liczba % i == 0)
            return false;
    }
    return true;
}

bool czyNp(int liczba)
{
    if (liczba % 2 == 0)
        return false;

    return true;
}

vector<int> roznica(int liczba)
{
    vector<int> arr;
    arr.push_back(0);
    arr.push_back(0);
    arr.push_back(0);

    for (int i = 2; i <= liczba - 2; i++)
    {
        for (int j = liczba - 2; j > 1; j--)
        {
            if ((czyNp(i) && czyPierwsza(i)) && (czyNp(j) && czyPierwsza(j)))
            {
                if (i + j == liczba)
                {
                    if (j - i >= arr[2])
                    {
                        arr[0] = i;
                        arr[1] = j;
                        arr[2] = j - i;
                    }
                }
            }
        }
    }
    return arr;
}

void wypiszNpPierwsze(ifstream &dataIn, ofstream &dataOut)
{
    int liczba;
    string slowo;

    dataOut << "1)" << endl;
    while (!dataIn.eof())
    {
        dataIn >> liczba >> slowo;
        if (liczba > 4)
        {
            vector<int> r = roznica(liczba);
            if (liczba != 0 && r[0] != 0 && r[1] != 0)
                dataOut << liczba << " " << r[0] << " " << r[1] << endl;
        }
    }
    dataIn.clear();
}

void wypiszSlowa(ifstream &dataIn, ofstream &dataOut)
{
    dataIn.seekg(0, ios::beg);
    int liczba;
    string slowo, maxPodciag, tempPodciag;

    dataOut << "2)" << endl;
    while (!dataIn.eof())
    {
        dataIn >> liczba >> slowo;
        tempPodciag = "";
        maxPodciag = "";

        for (int i = 0; i < slowo.length() - 1; i++)
        {
            if (i == 0)
                tempPodciag = slowo[i];

            if (slowo[i] == slowo[i + 1])
            {
                tempPodciag += slowo[i + 1];
                if (tempPodciag.length() > maxPodciag.length())
                    maxPodciag = tempPodciag;
            }
            else
                tempPodciag = slowo[i + 1];
        }
        if (tempPodciag.length() > maxPodciag.length())
            maxPodciag = tempPodciag;

        dataOut << maxPodciag << " " << maxPodciag.length() << endl;
    }
    dataIn.clear();
}

void wypiszPare(ifstream &dataIn, ofstream &dataOut)
{
    dataIn.seekg(0, ios::beg);
    int liczba2, minLiczba;
    string slowo2, minSlowo;

    dataIn >> minLiczba >> minSlowo;
    while (!dataIn.eof())
    {
        dataIn >> liczba2 >> slowo2;

        if ((minLiczba > liczba2) && (liczba2 == slowo2.length()))
        {
            minLiczba = liczba2;
            minSlowo = slowo2;
        }
        else if (minLiczba == liczba2)
        {
            int dl = minSlowo.length() > slowo2.length() ? slowo2.length() : minSlowo.length();

            for (int j = 0; j < dl; j++)
            {
                if ((minSlowo[j] > slowo2[j]) && (liczba2 == slowo2.length()))
                {
                    minSlowo = slowo2;
                    minLiczba = liczba2;
                    break;
                }
            }
        }
    }
    dataOut << "3)" << endl
            << minLiczba << " " << minSlowo << endl;
}